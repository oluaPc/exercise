git clone https://oluaPc@bitbucket.org/oluaPc/exercise.git

Duplicate file ".env.example" and name it ".env" (root folder)  
Create a new database in mysql and edit ".env" file accordly  
Example:  
DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=easypay  
DB_USERNAME=user  
DB_PASSWORD=password  

Run in console on the root folder of the project this commands:  
composer install  
php artisan key:generate  
php artisan passport:install  

And finally launch it:  
php artisan serve
  
  
  
endpoints:

http://localhost:8000/api/register POST  
name, email, password, password_confirmation  

http://localhost:8000/api/login POST  
email, password  
  
  
Use bearer token given by login:  
http://localhost:8000/api/customers  GET  

http://localhost:8000/api/customers POST  
name, email, phone, country  

http://localhost:8000/api/customers/1 GET

http://localhost:8000/api/customers/1 PUT  
name, email, phone, country  

http://localhost:8000/api/customers/1 DELETE
